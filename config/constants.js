var prodEnv = require('./prod.env')
var devEnv = require('./dev.env')
var testEnv = require('./test.env')

var apiUrl = null
if (process.env.NODE_ENV === 'production') {
    var apiUrl = prodEnv.API_URL
}
if (process.env.NODE_ENV === 'development') {
    var apiUrl = devEnv.API_URL
}
if (process.env.NODE_ENV === 'testing') {
    var apiUrl = testEnv.API_URL
}

export default {
    apiUrl: apiUrl,
    requestSuccess: 'success'
}