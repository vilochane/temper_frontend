// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from "vue";
import App from "./App";
import router from "./router";
import VeeValidate from "vee-validate";
import {dictionary} from "./utility/Validator/dictionary";
import BootstrapVue from "bootstrap-vue";
import store from "../src/store";
import "bootstrap/dist/css/bootstrap.css";
import "bootstrap-vue/dist/bootstrap-vue.css";
//styles
//end of styles

//use
Vue.use(VeeValidate, {
    locale: 'en',
    dictionary
})
Vue.use(BootstrapVue)
Vue.use(require('moment'))
//end of use

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
    el: '#app',
    router,
    store,
    template: '<App/>',
    components: {App}
})



