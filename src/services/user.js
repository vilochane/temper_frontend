import {http} from "./http-request";
import store from "../store";

export default {

    // User object will let us check authentication status
    data: {
        loading: false,
        user: store.state.user
    },
    // Send a request to the login URL and save the returned JWT
    login(component, credentials, redirect) {
        var url = 'login'
        http.post(url, {
            data: credentials
        }).then(response => {
            if (response.status === store.state.constants.requestSuccess
            ) {

            }
            console.log(response.data)
        }).catch(e => {
            //this.errors.push(e)
        })
    }
}