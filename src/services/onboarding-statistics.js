import {http} from "./http-request";
export default {
    data: {},
    onBoardingStatistics(component){
        var url = 'report/onboarding-statistics'
        return http.get(url).then(response => {
            return response.data.data.onboard_statistics
        }).catch(e => {
            var data = e.response.data
            if (data.code === 422 && Object.keys(data.errors.messages).length > 0) {
                for (let column in data.errors.messages) {
                    component.errors.add(column, data.errors.messages[column], 'server')
                }
            }
        })

    }
}