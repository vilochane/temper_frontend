import axios from 'axios';
import store from '../store'
import constants from '../../config/constants'

var user = store.state.user;
var options = {
    baseURL: constants.apiUrl,
    headers: {
        'Content-Type': 'applicaiton/json'
    }
}

if (user.authenticated) {
    options.headers.Authorization = 'Bearer {user.token}'
}

export const http = axios.create(options)