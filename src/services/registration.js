import {http} from "./http-request";
import store from "../store";

export default {

   
    data: {
        errors: [],
        profile: store.state.user.profile
    },
    
    businessCard(component, profile, redirect) {
        store.state.user.profile = {first_name: profile.first_name}
        component.$router.push(redirect)
    },
    whoAreYou(component, profile, redirect){
        var url = 'user'
        store.state.user.profile = profile
        http.post(url, {
            data: profile
        }).then(response => {
            component.$store.state.user.profile = response.data.data.user
            component.$router.push(redirect)
        }).catch(e => {
            var data = e.response.data
            if (data.code === 422 && Object.keys(data.errors.messages).length > 0) {
                for (let column in data.errors.messages) {
                    component.errors.add(column, data.errors.messages[column], 'server')
                }
            }
            console.log(component.errors)
        })

    }
}