import Vue from "vue";
import Router from "vue-router";
import Home from "@/components/Home";
import Login from "@/components/Login/Login";
import VerifyLogin from "@/components/Login/VerifyLogin";
import RegistrationPortal from "@/components/Registration/Portal";
import RegistrationBusinessCard from "@/components/Registration/BusinessCard";
import RegistrationWhoAreYou from "@/components/Registration/WhoAreYou";
import RegistrationSuccess from "@/components/Registration/SuccessMail";

Vue.use(Router)

export default new Router({
    routes: [
        {
            path: '/',
            name: 'Home',
            component: Home
            //beforeEnter: requireAuth,
        },
        {
            path: '/Login',
            name: 'Login',
            component: Login
            //beforeEnter: requireAuth,
        },
        {
            path: '/verify-login',
            component: VerifyLogin,
        },
        //registration
        {
            path: '/registration-portal',
            component: RegistrationPortal,
        },
        {
            path: '/registration-business-card',
            component: RegistrationBusinessCard,
        },
        {
            path: '/registration-who-are-you',
            component: RegistrationWhoAreYou,
        },
        {
            path: '/registration-success',
            component: RegistrationSuccess,
        },
        //end of registration
    ]
})
