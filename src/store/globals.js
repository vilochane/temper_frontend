export default {
    setGlobalVar (name, value) {
        localStorage.setItem(name, value)
    },
    getGlobalVar (name) {
        return localStorage.getItem(name)
    }

}