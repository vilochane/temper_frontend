import Vue from "vue";
import Vuex from "vuex";
import systemConstants from "../../config/constants";
//modules states
//end of modules states

Vue.use(Vuex)

const lists = {
    title: [
        {
            'id': 1,
            'name': 'Mr.'
        },
        {
            'id': 2,
            'name': 'Ms.'
        },
        {
            'id': 3,
            'name': 'Mrs.'
        },
        {
            'id': 4,
            'name': 'Miss'
        },

    ]
}

const moduleUser = {
    state: {
        userId: null,
        userRole: null,
        username: null,
        authenticated: false,
        token: null,
        profile: {first_name: null, email: null}
    }
}
const moduleConstants = {
    state: systemConstants
}
const moduleLists = {
    state: lists
}


export default new Vuex.Store({
    modules: {
        user: moduleUser,
        constants: moduleConstants,
        lists: moduleLists
    }
})