export const dictionary = {
    en: {
        attributes: {
            user_title: 'Title',
            first_name: 'First Name',
            last_name: 'Last Name',
            postal_code: 'Postal Code',
            house_number: 'House Number',
            addition: 'Addition',

        }
    }
};